// Don't forget to change the pathFile if you change the position of the file
var pathFile = './public/files/';
var yamlFile = 'micado_types.yml'; 

const fs = require('fs');
const yaml = require('js-yaml');
const fetch = require("node-fetch");

// Load modules
var express = require('express');
var app = express();
// First parameter below is the port
var serv = app.listen(8080, function() {
    console.log("Waiting for a connection at localhost:8080...");
});

// Express configuration to use the public repertory
app.use(express.static('public'));
// set up to 
app.get('/', function(req, res) {  
    res.sendFile(__dirname + '/public/gui.html');
});

function Server(serv) {
	// Listen on the websockets
	this.io = require('socket.io').listen(serv);
}

Server.prototype.initSocket = function(socket) {
		// Debug message
		console.log("Client connected");

		//socket.on
		socket.on("parseFile", function(){
			try {
				// Parsing the yaml file
				var doc = yaml.safeLoad(fs.readFileSync(pathFile + yamlFile, 'utf8'));

				//var dump_description = yaml.dump(doc['interface_types']['tosca.interfaces.MiCADO.Occopus']['description']);

				// Get the different names of the 'interface_types' 
				var tabInterface = [];

				for (var key in doc['interface_types']) {
					if (doc['interface_types'].hasOwnProperty(key)) {
						tabInterface.push(doc['interface_types'][key]);
					}
				}
				socket.emit("sendInterfaceTypes", tabInterface);
				
				// Get the different names of the 'node_types' 
				var tabNode = [];

				for (var key in doc['node_types']) {
					if (doc['node_types'].hasOwnProperty(key)) {
						tabNode.push(doc['node_types'][key]);
					}
				}
				socket.emit("sendNodeTypes", tabNode);				
			} catch (e) {
				console.log(e);
				return e;
			}
		});

		// For the buttons, research with the description on the node
		socket.on("addBtn", function(name){
			try{
				var doc = yaml.safeLoad(fs.readFileSync(pathFile + yamlFile, 'utf8'));
				var resBtn = [];
				var node;
				for (var key1 in doc){
					for (var key2 in doc[key1]) {
						if (doc[key1][key2]['description'] == name) {
							node = doc[key1][key2];
						}
					}
				}


				for (var key in node) {
					if (node.hasOwnProperty(key) && key != "derived_from" && key != "description") {
						resBtn.push(key);
					}
				}

				socket.emit("resButtons", resBtn);

	/*		Options data

				for (var key in node) {
					if (node.hasOwnProperty(key) && key != "derived_from" && key != "description") {
						for (var key2 in node[key]) {
							if (node[key][key2]['required'] == false) {
								res2Btn.push(key);
							}
						}
					}
				}
	*/
			} catch (e) {
				console.log(e);
				return e;
			}
		});
}

// Initialization and connection to the server
var server = new Server(serv);
server.io.on('connection', function(soc) {
	server.initSocket(soc);
});

