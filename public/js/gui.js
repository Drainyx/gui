// Connection of the socket in the client side
var socket = io.connect();

document.addEventListener("DOMContentLoaded", function(e) {
	
	socket.emit("parseFile");
	
	// Get the different types and add them to the GUI
	socket.on("sendInterfaceTypes", function(data) {
		if (data.length > 0) {
			document.getElementById("toolbox").innerHTML += '<h3>Interfaces types</h3>';
		}
		for (var i = 0; i < data.length; ++i) {
			addTask("interface" ,data[i]);
		}
	});

	socket.on("sendNodeTypes", function(data) {
		if (data.length > 0) {
			document.getElementById("toolbox").innerHTML += '<h3>Node types</h3>';
		}
		for (var i = 0; i < data.length; ++i) {
			addTask("node", data[i]);
		}
	});


});

// The function addTask add tasks to the toolbox (left zone)
var countTask = 0;
var tabTask = [];

function addTask(group = null, task = null) {

	if (group == null) {
		if (document.querySelectorAll(".other").length == 0) {
			document.getElementById("toolbox").innerHTML += '<h3>Other types</h3>';
		}
		document.getElementById("toolbox").innerHTML += '<div id="'+countTask+'" class="other" draggable="true" ondragstart="drag(event)"><p><i>'+ getTaskName(countTask)+'</i></p></div>';
	}
	else {
		document.getElementById("toolbox").innerHTML += '<div id="'+countTask+'" class="'+group+'" draggable="true" ondragstart="drag(event)"><p><i>'+ task['description'] +' </i></p></div>';		
	}

	tabTask.push(task);
	++countTask;
}

// Ask a task name to the user
function getTaskName(nb) {
	var ret = prompt("Task name: ", "task"+nb);
	return ret == null ? nb : ret;
}

//************************************* Drag & Drag (& Copy) *****************************************//

function allowDrop(ev) {
	ev.preventDefault();
}

function drag(ev) {	
	ev.dataTransfer.setData("srcId", ev.target.id);
}

function drop(ev) {
	ev.preventDefault();
	var data = ev.dataTransfer.getData("srcId");
	// If you want to move a task from the toolbox, use a drag & copy
	// Else, a simple drag & drop
	if (document.getElementById(data).parentNode.id == "toolbox") {
		var nodeCopy = document.getElementById(data).cloneNode(true);
		nodeCopy.id = countTask;
		++countTask;
		ev.target.appendChild(nodeCopy);
		addButtons(nodeCopy.id);
	}
	else{
		ev.target.appendChild(document.getElementById(data));
	}
}

//Remove tasks
function deleteDrop(ev) {
	ev.preventDefault();
	var srcId = ev.dataTransfer.getData("srcId");
	var element = document.getElementById(srcId);
	// You can't delete predifined tasks
	if (element.parentNode.id == "toolbox" && element.className != "other") {
		return;
	}
	element.parentNode.removeChild(element);
}

//**************************************************************************************************//

function addButtons(node) {
	socket.emit("addBtn", node.innerText);
	socket.on("resButtons", function(data) {
	console.log(node);
		for (var i = 0; i < data.length; ++i) {
			document.getElementById(node).innerHTML += '<input type="button" class="btnAdd" value="+ '+data[i]+'" onclick="btnAdd(event)" title="Optional '+data[i]+'"><div class="list" id="'+data[i]+'"><h4>Optional '+data[i]+'</h4></div>';
		}
	});
}

// Optional button
function btnAdd(ev) {
	var sibling = ev.target.nextSibling;
	if (sibling.className == "list") {
		if (sibling.style.display == 'block') {
			sibling.style.display ='none';
		}
		else {
			sibling.style.display ='block';
		}
	}
}

// Parse the GUI into a yaml file (need to transform the GUI into a tab)
function convertToFile(){
	console.log("convert");
}